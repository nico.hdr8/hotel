import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created: 13.02.2023
 *
 * @author Nico Haider (nicoh)
 */
public class Hotel implements Comparable<Hotel> {
    private String name;
    private String location;
    private int size;
    private boolean smoking;
    private int rate;
    private LocalDate date;
    private String owner;

    public static void main(String[] args) {
        try (RandomAccessFile raf = new RandomAccessFile("src/main/resources/hotels.db", "r")) {
            raf.skipBytes(76);
            byte[] bytes = new byte[159];
            raf.read(bytes);
            System.out.println(Arrays.toString(bytes));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Hotel(String name, String location, int size, boolean smoking, int rate, LocalDate date, String owner) {
        this.name = name;
        this.location = location;
        this.size = size;
        this.smoking = smoking;
        this.rate = rate;
        this.date = date;
        this.owner = owner;
    }

    public Hotel(byte[] data, Map<String, Short> columns) {
        List<String> parameters = new ArrayList<>();
        int index = 0;
        for (String current : columns.keySet()) {
            byte[] bytes = new byte[columns.get(current)];
            for (int i = 0; i < columns.get(current); i++) {
                bytes[i] = data[index];
                index++;
            }
            String currentString = new String(bytes, StandardCharsets.UTF_8);
            parameters.add(currentString.trim());
        }
        this.name = parameters.get(0);
        this.location = parameters.get(1);
        this.size = Integer.parseInt(parameters.get(2));
        this.smoking = parameters.get(3).equals("Y");
        this.rate = Integer.parseInt(parameters.get(4).substring(1, parameters.get(4).length() - 3)) * 100;
        this.date = LocalDate.parse(parameters.get(5), DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        this.owner = parameters.get(6);
    }

    public static Map<String, Short> readColumns(String filename) throws IOException {
        Map<String, Short> columns = new LinkedHashMap<>();
        try (RandomAccessFile raf = new RandomAccessFile(filename, "r")) {
            int id = raf.readInt();
            int offset = raf.readInt();
            short columnCount = raf.readShort();

            for (int i = 0; i < columnCount; i++) {
                short columnsNameLength = raf.readShort();
                byte[] bytes = new byte[columnsNameLength];
                raf.read(bytes);
                String columnName = new String(bytes, StandardCharsets.UTF_8);
                Short columnSize = raf.readShort();
                columns.put(columnName, columnSize);
            }

            return columns;
        }
    }

    public static Set<Hotel> readHotels(String filename) throws IOException {
        Set<Hotel> hotels = new TreeSet<>();
        Map<String, Short> columns = readColumns(filename);
        try (RandomAccessFile raf = new RandomAccessFile(filename, "r")) {
            int id = raf.readInt();
            int offset = raf.readInt();
            raf.skipBytes(offset - 8);
            while (raf.length() - raf.getFilePointer() > 0) {
                int checkDelete = raf.readUnsignedShort();
                if (checkDelete == 0x8000) {
                    raf.skipBytes(159);
                } else if (checkDelete == 0x0000) {
                    byte[] bytes = new byte[159];
                    raf.read(bytes);
                    hotels.add(new Hotel(bytes, columns));
                } else {
                    throw new IllegalArgumentException("wrong checkDelete in file " + filename);
                }
            }
        }
        return hotels;
    }


    public static int getStartingOffset(String filename) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile(filename, "r")) {
            raf.readInt();
            return raf.readInt();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return size == hotel.size && smoking == hotel.smoking && rate == hotel.rate && Objects.equals(name, hotel.name) && Objects.equals(location, hotel.location) && Objects.equals(date, hotel.date) && Objects.equals(owner, hotel.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, location, size, smoking, rate, date, owner);
    }

    @Override
    public int compareTo(Hotel o) {
        if (o.location.compareTo(this.location) < 0) {
            return 1;
        }
        else if (o.location.compareTo(this.location) > 0){
            return -1;
        }
        else {
            return -o.name.compareTo(this.name);
        }
    }
}
